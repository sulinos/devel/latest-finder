import requests
import config

def fetch(url):
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0',
            'Accept-Language': 'en-US,en;q=0.5',
            'DNT': '1',
            'Accenpt': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'PRIVATE-TOKEN': config.gitlab_token
        }
        response = requests.get(url)
        if response.status_code == 200:
            return response.content.decode("UTF-8")
        return None
    except:
        return None
