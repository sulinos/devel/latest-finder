def link2ver(link):
    link = link.split("/")[-1]
    ver = ""
    e = False
    for c in link:
        if c in "0123456789":
            e = True
            ver += c
        elif c in ".-" and e:
            ver += c
        else:
            e = False
    if ver[-1] in ".-":
        ver = ver[:-1]
    return ver

def line2link(line):
    return line.split("href=\"")[1].split("\"")[0]
