from util.link import *
from util.fetcher import fetch
import subprocess, config
import json

def regex(ctx):
    ret = []
    for line in ctx.split("\n"):
        if "href=" in line and "tar.gz" in line:
            link = "https://gitlab.com"+line.split("href=\"")[1].split("\"")[0]
            version = link2ver(link)
            ret.append((link,version))
    return ret


def search(name):
    ret = []
    ctx = subprocess.getoutput(
        "curl --header \"PRIVATE-TOKEN: {}\" \"https://gitlab.com/api/v4/search?scope=projects&search={}\" 2>/dev/null".format(
            config.gitlab_token,
            name
        ))
    j = json.loads(ctx)
    for item in j:
       ret.append("https://gitlab.com/"+item["path_with_namespace"])
    return ret
