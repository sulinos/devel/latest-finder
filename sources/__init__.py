import sources.gitlab as gitlab
import sources.github as github

def regex(ctx):
    if not ctx:
        return
    if "content=\"GitLab\"" in ctx:
        return gitlab.regex(ctx)
    if "name=\"hostname\" content=\"github.com\"" in ctx:
        return github.regex(ctx)

def search(name):
    links = []
    links += github.search(name)
    links += gitlab.search(name)
    return links
