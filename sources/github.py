from util.link import *
from util.fetcher import fetch

def regex(ctx):
    ret = []
    for line in ctx.split("\n"):
        if "href=" in line and "tar.gz" in line:
            link = "https://github.com"+line2link(line)
            version = link2ver(link)
            ret.append((link,version))
    return ret

def search(name):
    ret = []
    ctx = fetch("https://github.com/search?q="+name)
    if not ctx:
        return ret
    for line in ctx.split("\n"):
        if "href=" in line and "class=\"v-align-middle\"" in line:
            ret.append("https://github.com"+line2link(line))
    return ret
